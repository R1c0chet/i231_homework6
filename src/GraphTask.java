/**
 * Task nr 8
 * Compose a method to find a complement graph to a random simple graph
 *
 * @author Jaanus Pöial (author of the code base)
 * @author Jan Eerik (DK21)
 */

public class GraphTask {

   public static void main(String[] args) {
      GraphTask a = new GraphTask();
      a.run();
   }

   private void run() {
      Graph g = new Graph("G");

//      Tests

//      Test 1 - simple random graph of 4 vertices and 4 arcs
      g.createRandomSimpleGraph(4, 4);
      System.out.println(g);
      Graph O = g.createComplement();
      System.out.println(O);

//      Test 2 - a bigger graph of 15 vertices and 30 arcs
//      g.createRandomSimpleGraph(15, 30);
//      System.out.println(g);
//      Graph O = g.createComplement();
//      System.out.println(O);

//      Test 3 - testing a single vertex
//      g.createRandomSimpleGraph(1, 1);
//      System.out.println(g);
//      Graph O = g.createComplement();
//      System.out.println(O);

   }
   private class Vertex {

      private String id;
      private Vertex next;
      private Arc first;
      private int info = 0;

      Vertex(String s, Vertex v, Arc e) {
         id = s;
         next = v;
         first = e;
         info = 0;
      }

      Vertex(String s) {
         this(s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }

      private String getId() {
         return this.id;
      }

      private Vertex getNextVertex() {
         return this.next;
      }

      private void setNextVertex(Vertex v) {
         this.next = v;
      }
   }

   private class Arc {

      private String id;
      private Vertex target;
      private Arc next;
      private int info = 0;

      Arc(String s, Vertex v, Arc a) {
         id = s;
         target = v;
         next = a;
      }

      Arc(String s) {
         this(s, null, null);
      }

      @Override
      public String toString() {
         return id;
      }
   }

   private class Graph {

      private String id;
      private Vertex first;
      private int info = 0;

      Graph(String s, Vertex v) {
         id = s;
         first = v;
      }

      Graph(String s) {
         this(s, null);
      }

      private String getId() {
         return this.id;
      }

      private Vertex getFirst() {
         return this.first;
      }

      private void setFirst(Vertex f) {
         this.first = f;
      }

      @Override
      public String toString() {
         String nl = System.getProperty("line.separator");
         StringBuilder sb = new StringBuilder(nl);
         sb.append(id);
         sb.append(nl);
         Vertex v = first;
         while (v != null) {
            sb.append(v.toString());
            sb.append(" -->");
            Arc a = v.first;
            while (a != null) {
               sb.append(" ");
               sb.append(a.toString());
               sb.append(" (");
               sb.append(v.toString());
               sb.append("->");
               sb.append(a.target.toString());
               sb.append(")");
               a = a.next;
            }
            sb.append(nl);
            v = v.next;
         }
         return sb.toString();
      }

      private Vertex createVertex(String vid) {
         Vertex res = new Vertex(vid);
         res.next = first;
         first = res;
         return res;
      }

      private Arc createArc(String aid, Vertex from, Vertex to) {
         Arc res = new Arc(aid);
         res.next = from.first;
         from.first = res;
         res.target = to;
         return res;
      }

      /**
       * Create a connected undirected random tree with n vertices.
       * Each new vertex is connected to some random existing vertex.
       *
       * @param n number of vertices added to this graph
       */
      private void createRandomTree(int n) {
         if (n <= 0)
            return;
         Vertex[] varray = new Vertex[n];
         for (int i = 0; i < n; i++) {
            varray[i] = createVertex("v" + String.valueOf(n - i));
            if (i > 0) {
               int vnr = (int) (Math.random() * i);
               createArc("a" + varray[vnr].toString() + "_"
                       + varray[i].toString(), varray[vnr], varray[i]);
               createArc("a" + varray[i].toString() + "_"
                       + varray[vnr].toString(), varray[i], varray[vnr]);
            }
         }
      }

      /**
       * Create an adjacency matrix of this graph.
       * Side effect: corrupts info fields in the graph
       *
       * @return adjacency matrix
       */
      private int[][] createAdjMatrix() {
         info = 0;
         Vertex v = first;
         while (v != null) {
            v.info = info++;
            v = v.next;
         }
         int[][] res = new int[info][info];
         v = first;
         while (v != null) {
            int i = v.info;
            Arc a = v.first;
            while (a != null) {
               int j = a.target.info;
               res[i][j]++;
               a = a.next;
            }
            v = v.next;
         }
         return res;
      }

      /**
       * Create a connected simple (undirected, no loops, no multiple
       * arcs) random graph with n vertices and m edges.
       *
       * @param n number of vertices
       * @param m number of edges
       */
      private void createRandomSimpleGraph(int n, int m) {
         if (n <= 0)
            return;
         if (n > 2500)
            throw new IllegalArgumentException("Too many vertices: " + n);
         if (m < n - 1 || m > n * (n - 1) / 2)
            throw new IllegalArgumentException
                    ("Impossible number of edges: " + m);
         first = null;
         createRandomTree(n);       // n-1 edges created here
         Vertex[] vert = new Vertex[n];
         Vertex v = first;
         int c = 0;
         while (v != null) {
            vert[c++] = v;
            v = v.next;
         }
         int[][] connected = createAdjMatrix();
         int edgeCount = m - n + 1;  // remaining edges
         while (edgeCount > 0) {
            int i = (int) (Math.random() * n);  // random source
            int j = (int) (Math.random() * n);  // random target
            if (i == j)
               continue;  // no loops
            if (connected[i][j] != 0 || connected[j][i] != 0)
               continue;  // no multiple edges
            Vertex vi = vert[i];
            Vertex vj = vert[j];
            createArc("a" + vi.toString() + "_" + vj.toString(), vi, vj);
            connected[i][j] = 1;
            createArc("a" + vj.toString() + "_" + vi.toString(), vj, vi);
            connected[j][i] = 1;
            edgeCount--;  // a new edge happily created
         }
      }

      /**
       * Main method to copy the vertices from original graph and create complement arcs through adjacency matrix
       * @return complement graph
        */
      private Graph createComplement() {

         Graph C = new Graph("Complement of " + getId());

         int[][] originalMatrix = createAdjMatrix();
         int n = vertexCount();                          // number of vertices in original graph
         Vertex v = getFirst();

         // Clone vertices from the original graph
         Vertex[] vertices = new Vertex[n];
         int c = 0;
         while (v != null) {
            vertices[c] = cloneVertex(v);
            if (c > 0) vertices[c-1].setNextVertex(vertices[c]);
            c++;
            v = v.getNextVertex();
         }

         C.setFirst(vertices[0]);      // Set first vertex of complement graph

         //Create arcs based on the original adjacency matrix
         for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
               if (originalMatrix[i][j] == 0 && i != j) {      // i != j => only create arcs if vertices are different
                  createArc("a" + vertices[i].toString() + "_" + vertices[j].toString(), vertices[i], vertices[j]);
               }
            }
         }
         return C;
      }

      /**
       * Helper method to count vertices in the original graph
       * @return number of vertices
        */
      private int vertexCount() {

         int count = 0;
         for (Vertex v = getFirst(); v != null; v = v.getNextVertex()) {
            count++;
         }
         return count;
      }

      /**
       * Method to clone original vertex with its id
       * @param v vertex to be cloned
       * @return clone of vertex v
        */
      private Vertex cloneVertex(Vertex v) {
         return new Vertex(v.getId());
      }
   }
}






